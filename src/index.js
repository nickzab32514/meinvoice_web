import React, { Component } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route } from "react-router-dom";
import User from "./page/User";
import ReportInvoice from "./page/ReportInvoice";
import Infomation from "./page/Infomation";
import "antd/dist/antd.css";
const MainRouting = (
  <BrowserRouter>
    <Route path="/" component={User} exact={true} />
    <Route path="/reportInvoice" component={ReportInvoice} />
    <Route path="/information" component={Infomation} />
  </BrowserRouter>
);

ReactDOM.render(MainRouting, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
