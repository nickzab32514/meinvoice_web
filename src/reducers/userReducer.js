import {
  SEARCH_USERS,
  FETCH_USER_BEGIN,
  FETCH_USER_SUCCESS,
  FETCH_USER_ERROR,
} from "../action/userAction";

const initialState = {
  users: [],
  searchUsers: "",
  loading: false,
  error: "",
};

export const UserReducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_USERS:
      return {
        ...state,
        searchUsers: action.payLoad,
      };
    case FETCH_USER_BEGIN:
      return {
        ...state,
        loading: true,
      };
    case FETCH_USER_SUCCESS:
      return {
        ...state,
        users: action.payLoad,
        loading: false,
        error: "",
      };
    case FETCH_USER_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payLoad,
      };
    default:
      return state;
  }
};
