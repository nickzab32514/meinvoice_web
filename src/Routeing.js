import React from "react";
import "./App.css";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import User from "./page/User";
// import ToDopage from "./page/ToDopage";
import Login from "./page/Login";
import Information from "./page/Infomation";
import PrivateRoute from "./components/PrivateRoute";
import "antd/dist/antd.css"; // or 'antd/dist/antd.less'
import Header from "./page/Header";

const Routeing = () => {
  return (
    <BrowserRouter>
      {/* 
            <Route path="/" component={Header} /> */}
      <Route path="/user" component={User} exact={true} />
      <Route path="/information" component={Information} exact={true} />
      {/* <Route path="/todo/:user_id" component={ToDopage} /> */}
    </BrowserRouter>
  );
};
export default Routeing;
