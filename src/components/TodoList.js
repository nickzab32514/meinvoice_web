import React from 'react';
import { useSelector, connect, useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { searchTodo, doneAndDoing } from '../action/todoAction'

const TodoList = (props) => {
    const dispatch = useDispatch();

    const OnFilter = () => {
        if (filterData == 'All') {
            return todos
        } else {
            return todos.filter((value) => {
                return value.completed.toString() == filterData
            })
        }
    }
    const onGetTodo = (index) => {
        dispatch(doneAndDoing(index))
    }
    // const todos = useSelector(state => state.todoCompState.data)
    const { todos, searchData } = props
    // const searchData = useSelector(state => state.todoCompState.searchText)
    const filterData = useSelector(state => state.todoCompState.filterType)
    return (
        <div>
            <h3>Todo List </h3>
            <center>
                <table>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    {
                        searchData == '' ?
                            OnFilter().map((todo) => {
                                return (

                                    <tbody>
                                        <tr>
                                            <td>{todo.id}</td>
                                            <td>{todo.title}</td>
                                            <td>{
                                                todo.completed == true ?
                                                    "Done"
                                                    :
                                                    <button onClick={() => { onGetTodo(todo.id) }}>Doing</button>

                                            }</td>
                                        </tr>
                                    </tbody>
                                )
                            }
                            )
                            :
                            OnFilter().filter(data => data.title.match(searchData)).map((todo) => {
                                return (

                                    <tbody>
                                        <tr>
                                            <td>{todo.userId}</td>
                                            <td>{todo.title}</td>
                                            <td>{
                                                todo.completed == true ?
                                                    "Done"
                                                    :
                                                    <button onClick={() => { onGetTodo(todo.id) }}>Doing</button>
                                            }</td>
                                        </tr>
                                    </tbody>
                                )
                            })
                    }
                </table>
            </center>
        </div >

    )
}

const mapStateToprops = state => {
    return {
        todos: state.todoCompState.data,
        searchData: state.todoCompState.searchText
    }
}
const mapDispatchToProps = dispatch => {
    return bindActionCreators({ searchTodo }, dispatch)
}
export default connect(mapStateToprops, mapDispatchToProps)(TodoList)