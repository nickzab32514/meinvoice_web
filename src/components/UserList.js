import React from "react";
// import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { List, Row, Col, Tag, Icon } from "antd";
import { faBuilding, faUserTie } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const UserList = () => {
  // const userList = useSelector((state) => state.UserCompState.users);
  // const userSearch = useSelector((state) => state.UserCompState.searchUsers);

  const history = useHistory();
  const data = [
    {
      title: "My customer company 1",
    },
    {
      title: "My customer company  2",
    },
    {
      title: "My customer company  3",
    },
    {
      title: "My customer company  4",
    },
  ];
  // const Onsearch = () => {
  //   if (userSearch == "") {
  //     return userList;
  //   } else {
  //     return userList.filter((value) => {
  //       return value.name.match(userSearch);
  //     });
  //   }
  // };
  const OnClickTodo = (index) => {
    history.push("/todo/" + index);
  };
  return (
    <div className="contanier" style={{ margin: "8px 128px" }}>
      <List
        itemLayout="horizontal"
        dataSource={data}
        renderItem={(item) => (
          <Row
            style={{
              backgroundColor: "white",
              marginBottom: "16px",
              borderRadius: 5,
              boxShadow: "0px 3px 6px #EEEEEE",
            }}
          >
            <Col
              span={2}
              style={{
                backgroundColor: "#f1f1f1",
                borderRadius: 5,
                height: 98,
                textAlign: "left",
              }}
            >
              <Row style={{ marginLeft: 8, marginTop: 24 }}>
                <b
                  style={{ fontWeight: "600", marginLeft: 8, color: "#c1bebe" }}
                >
                  No.
                </b>
              </Row>
              <Row style={{ marginLeft: 8 }}>
                <b
                  style={{
                    fontWeight: "600",
                    marginLeft: 8,
                    fontSize: "28px/42px",
                  }}
                >
                  ID-234-56
                </b>
              </Row>
            </Col>
            <Col
              span={15}
              style={{ backgroundColor: "white", textAlign: "left" }}
            >
              <Row style={{ marginLeft: 24, marginTop: 16 }}>
                <FontAwesomeIcon
                  icon={faBuilding}
                  style={{ fontSize: 24, color: "rgba(0,0,0,.25)" }}
                />
                <b
                  style={{ fontWeight: "600", marginLeft: 8, color: "#666666" }}
                >
                  Dragon Enterpise
                </b>
              </Row>
              <Row style={{ marginLeft: 24, marginTop: 8 }}>
                {/* <Icon type="user" style={{ fontSize: 32, color: 'rgba(0,0,0,.25)' }} /> */}
                <FontAwesomeIcon
                  icon={faUserTie}
                  style={{ fontSize: 24, color: "rgba(0,0,0,.25)" }}
                />
                <b
                  style={{
                    fontWeight: "600",
                    marginLeft: 8,
                    paddingBottom: 10,
                    color: "#666666",
                  }}
                >
                  {item.title}
                </b>
              </Row>
            </Col>
            <Col span={7} style={{ backgroundColor: "white", borderRadius: 5 }}>
              <Tag
                style={{
                  backgroundColor: "#E7EFE9 ",
                  border: 0,
                  fontSize: 16,
                  marginTop: 24,
                  borderRadius: "40px",
                  padding: "0px 16px",
                }}
              >
                Issue date 14 april 2020
              </Tag>
              <Tag
                style={{
                  backgroundColor: "#A7CCB0",
                  border: 0,
                  color: "white",
                  fontSize: 16,
                  marginTop: 8,
                  marginBottom: 16,
                  borderRadius: "40px",
                  padding: "0px 16px",
                }}
              >
                Due date 14 april 2020
              </Tag>
            </Col>
          </Row>
        )}
      />
    </div>
  );
};
export default UserList;
