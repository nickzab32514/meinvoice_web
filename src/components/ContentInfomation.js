import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "./contentInfo.css";
import {
  Form,
  DatePicker,
  Table,
  Input,
  Row,
  Col,
  Button,
  Upload,
  Icon,
  Divider,
  Empty,
  Modal,
} from "antd";
import {
  faBuilding,
  faTrash,
  faEdit,
  faFileAlt,
  faTable,
  faUserTie,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ContentInformation = () => {
  const [loading, setLoading] = useState(false);
  const [visible1, setVisible1] = useState(false);
  const [visible2, setVisible2] = useState(false);

  // const userList = useSelector((state) => state.UserCompState.users);
  // const userSearch = useSelector((state) => state.UserCompState.searchUsers);
  const { TextArea } = Input;

  const dataSource = [
    {
      key: "1",
      no: "12345",
      description: "maker",
      qty: 10,
      unit_price: 32,
      total_price: 320,
    },
    {
      key: "2",
      no: "35789",
      description: "penco;",
      qty: 10,
      unit_price: 32,
      total_price: 320,
    },
  ];

  const columns = [
    {
      title: "No.",
      dataIndex: "no",
      key: "no",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Qty",
      dataIndex: "qty",
      key: "qty",
    },
    {
      title: "Unit price",
      dataIndex: "unit_price",
      key: "unit_price",
    },
    {
      title: "Total price",
      dataIndex: "total_price",
      key: "total_price",
    },
    {
      title: "",
      key: "action",
      render: (text, record) => (
        <span>
          <a>
            <FontAwesomeIcon
              icon={faEdit}
              className="styleIcon"
              onClick={showModalEdit}
            />
          </a>
          <Divider type="vertical" />
          <a>
            <FontAwesomeIcon icon={faTrash} className="styleIcon" />
          </a>
          <Modal
            visible={visible2}
            title="EDIT NEW ITEM"
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
              <Button
                style={{
                  marginRight: "124px",
                  width: 248,
                  height: 40,
                  boxShadow: " 0px 3px 6px #00000029",
                  borderRadius: "24px",
                  color: "#FFF",
                  background: "#74957C 0% 0% no-repeat padding-box",
                }}
              >
                Submit
              </Button>,
            ]}
          >
            <Form>
              <Form.Item>
                <span className="ant-form-text" id="detailForm">
                  Description
                </span>
                <TextArea rows={4} id="detailFormModal" />
              </Form.Item>
              <Form.Item>
                <span className="ant-form-text" id="detailForm">
                  Qty
                </span>
                <Input placeholder="Qty" id="detailFormModal" />
              </Form.Item>
              <Form.Item>
                <span className="ant-form-text" id="detailForm">
                  Unit Price
                </span>
                <Input placeholder="Unit Price" id="detailFormModal" />
              </Form.Item>
              <Form.Item>
                <Row>
                  <Col span={24}>
                    <span className="ant-form-text" id="detailForm">
                      Total Price
                    </span>
                  </Col>
                  <Col>
                    <span className="ant-form-text" id="detailForm">
                      0
                    </span>
                  </Col>
                </Row>
              </Form.Item>
            </Form>
          </Modal>
        </span>
      ),
    },
  ];
  const onChange = (date, dateString) => {
    console.log(date, dateString);
  };
  const showModal = () => {
    setVisible1(true);
  };
  const showModalEdit = () => {
    setVisible2(true);
  };

  const handleOk = () => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setVisible1(false);
      setVisible2(false);
    }, 3000);
  };

  const handleCancel = () => {
    setVisible1(false);
    setVisible2(false);
  };
  return (
    <div className="contanier" style={{ margin: "8px 128px" }}>
      <Row>
        <Row>
          <Col span={11}>
            <Row id="rowHeadTopic">
              <FontAwesomeIcon icon={faBuilding} className="styleIcon" />
              <span id="styletopic">Company Information</span>
            </Row>
          </Col>
          <Col span={1} />
          <Col span={11}>
            <Row id="rowHeadTopic">
              <FontAwesomeIcon icon={faUserTie} className="styleIcon" />
              <span id="styletopic">Your Customer</span>
            </Row>
          </Col>
        </Row>
        <Col
          span={11}
          className="card-form"
          style={{
            paddingLeft: 64,
            paddingRight: 64,
            boxShadow: "0px 3px 6px #00000029",
            borderRadius: "8px",
          }}
        >
          <Form>
            <Form.Item
              validateStatus="error"
              help="Should be combination of numbers and alphabets"
            >
              <span className="ant-form-text" id="detailForm">
                Name
              </span>
              <Input placeholder="Name" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                Tax Id
              </span>
              <Input placeholder="Tax Id" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                PhoneNumber
              </span>
              <Input placeholder="Name" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                E-mail Address
              </span>
              <Input placeholder="E-mail Address" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                Phone number
              </span>
              <Input placeholder="Name" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                Logo
              </span>
              <Row id="detailForm">
                <Col>
                  <Upload name="logo" action="/upload.do" listType="picture">
                    <Button id="detailForm">
                      <Icon type="upload" /> Click to upload
                    </Button>
                  </Upload>
                </Col>
              </Row>
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                Address
              </span>
              <TextArea rows={4} />
            </Form.Item>
          </Form>
        </Col>
        <Col
          span={11}
          className="card-form"
          style={{
            paddingLeft: 64,
            paddingRight: 64,
            marginLeft: 40,
            boxShadow: "0px 3px 6px #00000029",
            borderRadius: "8px",
          }}
        >
          <Form>
            <Form.Item
              validateStatus="error"
              help="Should be combination of numbers and alphabets"
            >
              <span className="ant-form-text" id="detailForm">
                Name
              </span>
              <Input placeholder="Name" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                Tax Id
              </span>
              <Input placeholder="Tax Id" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                PhoneNumber
              </span>
              <Input placeholder="Name" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                E-mail Address
              </span>
              <Input placeholder="E-mail Address" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                Phone number
              </span>
              <Input placeholder="Name" />
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                Logo
              </span>
              <Row id="detailForm">
                <Col>
                  <Upload name="logo" action="/upload.do" listType="picture">
                    <Button id="detailForm">
                      <Icon type="upload" /> Click to upload
                    </Button>
                  </Upload>
                </Col>
              </Row>
            </Form.Item>
            <Form.Item>
              <span className="ant-form-text" id="detailForm">
                Address
              </span>
              <TextArea rows={4} />
            </Form.Item>
          </Form>
        </Col>
      </Row>

      <Row style={{ marginTop: "16px !important" }}>
        <Col span={11} style={{ marginTop: 24 }}>
          <Row id="rowHeadTopic">
            <FontAwesomeIcon icon={faFileAlt} className="styleIcon" />
            <span id="styletopic" style={{ marginTop: "16px" }}>
              Invoice Information
            </span>
          </Row>
        </Col>
        <Col
          span={23}
          className="card-form"
          style={{
            marginTop: 24,
            paddingLeft: 64,
            paddingRight: 72,
            boxShadow: "0px 3px 6px #00000029",
            borderRadius: "8px",
          }}
        >
          <Form>
            <Row>
              <Col span={11}>
                <Form.Item>
                  <span className="ant-form-text" id="detailForm">
                    No.
                  </span>
                  <Input placeholder="Name" />
                </Form.Item>
                <Form.Item>
                  <span className="ant-form-text" id="detailForm">
                    Issue Date
                  </span>
                  <DatePicker style={{ width: 500 }} onChange={onChange} />
                </Form.Item>
                <Form.Item>
                  <span className="ant-form-text" id="detailForm">
                    Due Date
                  </span>
                  <DatePicker style={{ width: 500 }} onChange={onChange} />
                </Form.Item>
              </Col>
              <Col span={2} />
              <Col span={11}>
                <Form.Item>
                  <span className="ant-form-text" id="detailForm">
                    Note
                  </span>
                  <TextArea rows={11} />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Col>
      </Row>
      <Row style={{ marginTop: "16px !important" }}>
        <Col span={11} style={{ marginTop: 24 }}>
          <Row id="rowHeadTopic">
            <FontAwesomeIcon icon={faTable} className="styleIcon" />
            <span id="styletopic" style={{ marginTop: "16px" }}>
              Item
            </span>
          </Row>
        </Col>

        <Col
          span={23}
          className="card-form"
          style={{
            marginTop: 24,
            paddingLeft: 64,
            paddingRight: 72,
            boxShadow: "0px 3px 6px #00000029",
            borderRadius: "8px",
            textAlign: "right",
          }}
        >
          <Button onClick={showModal} className="addModal">
            Add New Item
          </Button>
          <Modal
            visible={visible1}
            title="ADD NEW ITEM"
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[<Button className="modalSubmit">Submit</Button>]}
          >
            <Form>
              <Form.Item>
                <span className="ant-form-text" id="detailForm">
                  Description
                </span>
                <TextArea rows={4} id="detailFormModal" />
              </Form.Item>
              <Form.Item>
                <span className="ant-form-text" id="detailForm">
                  Qty
                </span>
                <Input placeholder="Qty" id="detailFormModal" />
              </Form.Item>
              <Form.Item>
                <span className="ant-form-text" id="detailForm">
                  Unit Price
                </span>
                <Input placeholder="Unit Price" id="detailFormModal" />
              </Form.Item>
              <Form.Item>
                <Row>
                  <Col span={24}>
                    <span className="ant-form-text" id="detailForm">
                      Total Price
                    </span>
                  </Col>
                  <Col>
                    <span className="ant-form-text" id="detailForm">
                      0
                    </span>
                  </Col>
                </Row>
              </Form.Item>
            </Form>
          </Modal>
          <Form>
            <Form.Item>
              <Table dataSource={dataSource} columns={columns} />
            </Form.Item>
          </Form>
        </Col>
      </Row>
      <Row style={{ marginTop: 64 }}>
        <Col span={24}>
          <Button className="bttSubmit">Submit</Button>
        </Col>
      </Row>
    </div>
  );
};
export default ContentInformation;
