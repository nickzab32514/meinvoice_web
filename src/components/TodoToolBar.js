import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { searchTodo, filter } from '../action/todoAction'

const TodoToolBar = () => {

    // const [searchToDo, setSearchTodo] = useState('')

    const dispatch = useDispatch()

    const onSeach = (evnet) => {
        let textValue = evnet.target.value
        dispatch(searchTodo(textValue))
        // setSearchTodo(textValue)
    }

    const onFilter = (value) => {
        let textFilter = value.target.value
        dispatch(filter(textFilter))
    }

    return (
        <div>
            <input onChange={onSeach}></input>
            <select defaultValue={"All"} onChange={onFilter}>
                <option value="All">All</option>
                <option value={false}>Doing</option>
                <option value={true}>Done</option>
            </select>
        </div>
    )
}
export default TodoToolBar