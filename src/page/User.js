import React, { useEffect } from "react";
import UserList from "../components/UserList";
// import { useDispatch } from "react-redux";
// import { fetchUsers } from "../action/userAction";
import Header from "./Header";

const User = () => {
  // const dispatch = useDispatch();

  //   useEffect(() => {
  //     dispatch(fetchUsers());
  //   }, []);

  return (
    <div>
      <Header />
      <center style={{ marginTop: 48 }}>
        <UserList />
      </center>
    </div>
  );
};
export default User;
