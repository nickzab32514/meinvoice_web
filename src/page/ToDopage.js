import React, { useEffect } from 'react'
import TodoToolBar from '../components/TodoToolBar'
import TodoList from '../components/TodoList'
import { fetchTodos } from '../action/todoAction'
import { useDispatch } from 'react-redux'
import Header from './Header'

const ToDopage = (props) => {

    const dispatch = useDispatch()

    useEffect(() => {
        const id = props.match.params.user_id
        dispatch(fetchTodos(id))
    }, [])
    return (
        <div>
            <Header />
            <center>
                <h1>Todo Page</h1>
                <TodoToolBar />
                <TodoList />
            </center>

        </div>
    )
}
export default ToDopage