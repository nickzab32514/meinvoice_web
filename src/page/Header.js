import React, { useState, useEffect } from "react";
import { Row, Col } from "antd";
import { Link } from "react-router-dom";
import styled from "styled-components";
import meinvoiceImg from "../image/logo.PNG";

export default () => {
  const [index, setIndex] = useState(1);
  const Button = styled.div`
    margin-top: 5px;
    font-size: 24px;
    background: #fafafa 0% 0% no-repeat padding-box;
    border-radius: 8px;
    opacity: 1;
    margin-right: 10px;
    width: 100px;
    &:hover {
      background: #eeeeee;
      text-decoration: none;
    }
  `;
  const ButtonActive = styled.div`
    margin-top: 5px;
    font-size: 24px;
    background: #a7ccb0 0% 0% no-repeat padding-box;
    border-radius: 8px;
    opacity: 1;
    margin-right: 10px;
    width: 100px;
  `;
  const Text = styled.p`
    text-align: center;
  `;
  const LinkMenu = styled(Link)`
    color: black;
    text-decoration: none;
    &:hover {
      color: black;
      text-decoration: none;
      cursor: pointer;
    }
  `;
  const LinkMenuActive = styled(Link)`
    color: white;
    text-decoration: none;
    &:hover {
      color: black;
      text-decoration: none;
      cursor: pointer;
    }
  `;
  console.log(index);
  return (
    <Row justify="center" style={{ marginRight: 240 }}>
      <Col span={16}>
        <Row justify="space-around">
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Link to="/">
              <img src={meinvoiceImg} height="50" alt="" />
            </Link>
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Row gutter={8}>
              <Col className="gutter-row" style={{ float: "right" }}>
                {index == 1 ? (
                  <ButtonActive
                    onClick={() => {
                      setIndex(1);
                    }}
                  >
                    <LinkMenuActive to="/">
                      <Text>LIST</Text>
                    </LinkMenuActive>
                  </ButtonActive>
                ) : (
                  <Button
                    onClick={() => {
                      setIndex(1);
                    }}
                  >
                    <LinkMenu to="/user">
                      <Text>LIST</Text>
                    </LinkMenu>
                  </Button>
                )}
              </Col>
              <Col className="gutter-row">
                {index == 2 ? (
                  <ButtonActive
                    onClick={() => {
                      setIndex(2);
                    }}
                  >
                    <LinkMenuActive to="/information">
                      <Text>CREATE</Text>
                    </LinkMenuActive>
                  </ButtonActive>
                ) : (
                  <Button
                    onClick={() => {
                      setIndex(2);
                    }}
                  >
                    <LinkMenu to="/information">
                      <Text>CREATE</Text>
                    </LinkMenu>
                  </Button>
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
