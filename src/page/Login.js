import React from 'react'
import { Form, Icon, Input, Button, Card } from 'antd';

const Login = () => {
    return (
        <div style={{ padding: '30px' }}>
            <center>
                <Card style={{ background: '#88bbdd', width: 350 }}>
                    <h1>Login</h1>
                    <Form className="login-form">
                        <Form.Item>
                            <Input
                                prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                placeholder="Username"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Input
                                prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                type="password"
                                placeholder="Password"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button" onClick={() => window.location = "/processLogin"}>
                                Log in
          </Button>
                        </Form.Item>
                    </Form>
                </Card>
            </center>
        </div>
    )
}
export default Login