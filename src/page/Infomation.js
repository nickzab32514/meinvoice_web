import React, { useEffect } from "react";
// import UserToolBar from '../components/UserToolBar'
// import UserList from '../components/UserList'
import { useDispatch } from "react-redux";
// import { fetchUsers } from '../action/userAction'
import Header from "./Header";
import ContentInformation from "../components/ContentInfomation";

const Information = () => {
  //   const dispatch = useDispatch();

  //   useEffect(() => {
  //     dispatch(fetchUsers());
  //   }, []);

  return (
    <div>
      <Header />
      <center style={{ marginTop: 48, marginBottom: 48 }}>
        <ContentInformation />
      </center>
    </div>
  );
};
export default Information;
