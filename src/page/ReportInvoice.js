import React, { useEffect } from "react";
import { Layout, Row, Form, Col, Table, Empty } from "antd";
import "./report.css";
const { Content } = Layout;
const ReportInvoice = () => {
  const dataSource = [
    {
      key: "1",
      no: "12345",
      description: "maker",
      qty: 10,
      unit_price: 32,
      total_price: 320,
    },
    {
      key: "2",
      no: "35789",
      description: "penco;",
      qty: 10,
      unit_price: 32,
      total_price: 320,
    },
  ];

  const columns = [
    {
      title: "No.",
      dataIndex: "no",
      key: "no",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Qty",
      dataIndex: "qty",
      key: "qty",
    },
    {
      title: "Unit price",
      dataIndex: "unit_price",
      key: "unit_price",
    },
    {
      title: "Total price",
      dataIndex: "total_price",
      key: "total_price",
    },
  ];

  return (
    <div className="contanier">
      <Layout className="layout">
        <Content id="contentArea">
          <div className="site-layout-content">
            <Row style={{ textAlign: "left" }}>
              <Col span={6}>
                <Row className="areaCompany">
                  <Col>
                    <b>Dragon Enterprise</b>
                  </Col>
                  <Col>
                    <span>Ap #651-8679 Sodales Av. Tamuning PA 10855</span>
                  </Col>
                </Row>
                <Row className="areaCompany">
                  <Col>
                    <Row>
                      <b>TAX ID </b>
                      <span>0105535137307</span>
                    </Row>
                  </Col>
                  <Col>
                    <span>contact@dragon.com</span>
                  </Col>
                  <Col>
                    <span>089-724-2744</span>
                  </Col>
                </Row>
                <Row className="areaCompany">
                  <Col>
                    <b>BILL TO</b>
                  </Col>
                  <Col>
                    <b>My Customer Company</b>
                  </Col>
                  <Col>
                    <span>Ap #651-8679 Sodales Av. Tamuning PA 10855</span>
                  </Col>
                </Row>
                <Row className="areaCompany">
                  <Col>
                    <Row>
                      <b>TAX ID </b>
                      <span> 0304532136709</span>
                    </Row>
                  </Col>
                  <Col>
                    <span>contact@mycustomer.com</span>
                  </Col>
                  <Col>
                    <span>090-112-1567</span>
                  </Col>
                </Row>
              </Col>
              <Col span={12}></Col>
              <Col span={6}>
                <Row className="headerInv">
                  <b>Invoice</b>
                </Row>
                <Row>
                  <Col span={10}>
                    <Row className="atp1">
                      <Col>
                        <b>No.</b>
                      </Col>
                      <Col>
                        <b>Issue Date</b>
                      </Col>
                      <Col>
                        <b>Due Date</b>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={10}>
                    <Row className="atp2">
                      <Col>
                        <span>ID-2-127</span>
                      </Col>
                      <Col>
                        <span>24 March 2020</span>
                      </Col>
                      <Col>
                        <span>14 May 2020</span>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row className="cssTable">
              <Form>
                <Form.Item>
                  <Table
                    pagination={false}
                    dataSource={dataSource}
                    columns={columns}
                  />
                </Form.Item>
              </Form>
            </Row>
            <Row className="cssNote">
              <Col span={10}>
                <Row>
                  <b className="stNote">Note:</b>
                </Row>
                <Row>
                  <p className="paragrapNote">
                    Contrary to popular belief, Lorem Ipsum is not simply random
                    text. It has roots in a piece of classical Latin literature
                    from 45 BC, making it over 2000 years old. Richard
                    McClintock, a Latin professor at Hampden-Sydney College in
                    Virginia, looked up one of the more obscure Latin words,
                    consectetur, from a Lorem Ipsum passage, and going through
                    the cites of the word in classical literature, discovered
                    the undoubtable source. Lorem Ipsum comes from sections
                    1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The
                    Extremes of Good and Evil) by Cicero, written in 45 BC. This
                    book is a treatise on the theory of ethics, very popular
                    during the Renaissance. The first line of Lorem Ipsum,
                    "Lorem ipsum dolor sit amet..", comes from a line in section
                    1.10.32.
                  </p>
                </Row>
              </Col>
              <Col span={7} />
              <Col span={7}>
                <Row>
                  <Col span={10}>
                    <Row className="nt1">
                      <Col>
                        <span>Sub Total</span>
                      </Col>
                      <Col>
                        <span>Vat(%7)</span>
                      </Col>
                      <Col>
                        <span>Gran Total</span>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={10} style={{ marginLeft: "8px" }}>
                    <Row className="nt2">
                      <Col>
                        <b>280</b>
                      </Col>
                      <Col>
                        <b>19.6</b>
                      </Col>
                    </Row>
                  </Col>
                </Row>
                <Row>___________________________________</Row>
                <Row>
                  <Col span={10}>
                    <Row className="nt1">
                      <Col>
                        <span>Gran Total</span>
                      </Col>
                    </Row>
                  </Col>
                  <Col span={10} style={{ marginLeft: "8px" }}>
                    <Row className="nt2">
                      <Col>
                        <b>299.6</b>
                      </Col>
                    </Row>
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        </Content>
      </Layout>
    </div>
  );
};
export default ReportInvoice;
